export interface InviewPortType {
  distanceFromEnd: number;
  callback: () => boolean;
  target: HTMLElement;
}

const checkInViewIntersectionObserver = ({
  target,
  distanceFromEnd,
  callback,
}: InviewPortType) => {
  // eslint-disable-next-line no-undef
  const _funCallback: IntersectionObserverCallback = (
    // eslint-disable-next-line no-undef
    entries: IntersectionObserverEntry[],
    // eslint-disable-next-line no-undef
    observer: IntersectionObserver
  ) => {
    // eslint-disable-next-line no-undef
    entries.map((entry: IntersectionObserverEntry) => {
      if (entry.isIntersecting) {
        // NEED CALLBACK WILL RETURN BOOLEAN ---- IF TRUE WE WILL UNOBSERVER AND FALSE IS NO
        const unobserve = callback();
        unobserve && observer.unobserve(entry.target);
      }
      return true;
    });
  };

  // _checkBrowserSupport-----
  if (typeof window.IntersectionObserver === 'undefined') {
    return;
  }
  const options = {
    root: null,
    rootMargin: `${distanceFromEnd}px 0px`,
    threshold: 0,
  };
  // eslint-disable-next-line no-undef
  const observer = new IntersectionObserver(_funCallback, options);
  target && observer.observe(target);
};

export default checkInViewIntersectionObserver;
