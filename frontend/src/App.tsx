import React from 'react';
import Router from './router/index';
import './App.css';

function App() {
  return (
    <div className="bg-white text-base dark:bg-neutral-900 text-neutral-900 dark:text-neutral-200">
      <Router />
    </div>
  );
}

export default App;
