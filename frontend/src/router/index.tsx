import React from 'react';
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';
import Page404 from '../containers/Page404';

import PageHome from '../containers/PageHome';

const AppRoutes = () => {
  return (
    <BrowserRouter>
      {/*HEADER*/}
      <Routes>
        <Route path="/" element={<PageHome />} />
        <Route path="/#" element={<PageHome />} />
        <Route path="/home" element={<PageHome />} />
        <Route path="/404" element={<Page404 />} />
        <Route path="/slug/:id" element={<Page404 />} />
        <Route path="/post/:id" element={<Page404 />} />
        <Route path="*" element={<Navigate to="/404" />} />
      </Routes>
      {/*FOOTER*/}
    </BrowserRouter>
  );
};

export default AppRoutes;
