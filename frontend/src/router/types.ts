export interface LocationStates {
  '/'?: {};
  '/#'?: {};
  '/slug/:id'?: {};
  '/post/:id'?: {};
  '/404'?: {};
  '/home'?: {};
}

export type PathName = keyof LocationStates;