import pagesReducer from './pages';
import postReducer from './postSlice';

const rootReducers = {
  pages: pagesReducer,
  posts: postReducer,
};

export default rootReducers;
