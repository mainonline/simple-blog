import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import type { ThunkDispatch } from 'redux-thunk';
import { RootState } from '../store/store';
import { fetchPosts } from '../store/postSlice';
import SectionLatestPosts from './SectionLatestPosts';

const PageHome = () => {
  const dispatch = useDispatch<ThunkDispatch<never, never, never>>();
  const { posts, status, error } = useSelector(
    (state: RootState) => state.posts
  );

  useEffect(() => {
    dispatch(fetchPosts());
  }, [dispatch]);

  if (status === 'loading') {
    return <div>Loading...</div>;
  }
  if (status === 'failed') {
    return <div>{error}</div>;
  }
  return (
    <div className="nc-PageHome container relative">
      <SectionLatestPosts
        posts={posts}
        gridClass="sm:grid-cols-2"
        className="pb-16 lg:pb-28"
      />
    </div>
  );
};

export default PageHome;
