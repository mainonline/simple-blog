import React, { FC } from 'react';
import Heading from '../components/Heading';
import { PostDataType } from '../data/types';
import ButtonPrimary from '../components/Button/ButtonPrimary';
import Card from '../components/Card';

const postsDemo: PostDataType[] = [];
export interface SectionLatestPostsProps {
  posts?: PostDataType[];
  heading?: string;
  gridClass?: string;
  className?: string;
}

const SectionLatestPosts: FC<SectionLatestPostsProps> = ({
  posts = postsDemo,
  heading = 'Latest Articles 🎈',
  gridClass = '',
  className = '',
}) => {
  return (
    <div className={`nc-SectionLatestPosts relative ${className}`}>
      <div className="flex flex-col lg:flex-row">
        <div className="w-full lg:w-3/5 xl:w-2/3 xl:pr-14">
          <Heading>{heading}</Heading>
          <div className={`grid gap-6 md:gap-8 ${gridClass}`}>
            {posts.map(post => (
              <Card key={post.id} post={post} ratio="aspect-w-5 aspect-h-5" />
            ))}
          </div>
          <div className="flex flex-col mt-12 md:mt-20 space-y-5 sm:space-y-0 sm:space-x-3 sm:flex-row sm:justify-between sm:items-center">
            <ButtonPrimary>Show me more</ButtonPrimary>
          </div>
        </div>
        <div className="w-full space-y-7 mt-24 lg:mt-0 lg:w-2/5 lg:pl-10 xl:pl-0 xl:w-1/3 " />
      </div>
    </div>
  );
};

export default SectionLatestPosts;
