import React, { FC } from 'react';
import { PostDataType } from '../data/types';
import { Link } from 'react-router-dom';
export interface Card7Props {
  className?: string;
  post: PostDataType;
  hoverClass?: string;
  ratio?: string;
}

const Card: FC<Card7Props> = ({
  className = 'h-full',
  ratio = 'aspect-w-5 aspect-h-5 sm:aspect-h-7',
  post,
  hoverClass = '',
}) => {
  const { title, image, slug } = post;

  return (
    <div
      className={`nc-Card7 relative flex flex-col group rounded-3xl overflow-hidden ${hoverClass} ${className}`}
      data-nc-id="Card"
    >
      <div className="absolute inset-x-0 top-0 p-3  flex items-center justify-between transition-all opacity-0 z-[-1] group-hover:opacity-100 group-hover:z-10 duration-300" />
      <Link to={slug} className={`flex items-start relative w-full ${ratio}`}>
        <div>
          <img
            src={`http://localhost:5000/${image}`}
            className={className}
            alt={slug}
          />
        </div>
        <span className="absolute inset-0 bg-neutral-900 bg-opacity-20 opacity-0 group-hover:opacity-100 transition-opacity" />
      </Link>

      <div className="absolute bottom-3 inset-x-3 p-4 bg-white dark:bg-neutral-900 flex flex-col flex-grow rounded-3xl group-hover:shadow-2xl transition-shadow">
        <Link to={slug} className="absolute inset-0" />
        <div className="space-y-2.5 mb-3">
          {/*<CategoryBadgeList categories={categories} />*/}
          <h2 className="block text-base font-semibold text-neutral-900 dark:text-neutral-100 ">
            <Link to={slug} className="line-clamp-2" title={title}>
              {title}
            </Link>
          </h2>
        </div>
      </div>
    </div>
  );
};

export default Card;
