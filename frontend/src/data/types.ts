export interface CommentType {
  id: number;
  body: string;
  username: string;
  createdAt: string;
  updatedAt: string;
}

export interface PostDataType {
  id: number;
  title: string;
  content: string;
  author: string;
  createdAt: string;
  updatedAt: string;
  image: string;
  slug: string;
  comments: CommentType[];
}
