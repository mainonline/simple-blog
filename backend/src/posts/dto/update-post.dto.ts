import { Entity, Unique } from 'typeorm';
import { IsNotEmpty, IsString } from 'class-validator';

@Entity()
@Unique(['name'])
export class UpdatePostDto {
  @IsNotEmpty()
  @IsString()
  title: string;

  @IsNotEmpty()
  @IsString()
  content: string;

  @IsNotEmpty()
  @IsString()
  author: string;

  @IsString()
  slug: string;
}
