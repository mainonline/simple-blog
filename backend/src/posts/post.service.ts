import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Post } from './entities/post.entity';
import { Repository, QueryFailedError } from 'typeorm';
import { CreatePostDto } from './dto/create-post.dto';
import { UpdatePostDto } from './dto/update-post.dto';
import { UploadsService } from '../uploads/uploads.service';

@Injectable()
export class PostService {
  constructor(
    @InjectRepository(Post)
    private readonly postRepository: Repository<Post>,
    private uploadService: UploadsService,
  ) {}

  async createPost(createPostDto: CreatePostDto, image: any): Promise<Post> {
    try {
      const fileName = await this.uploadService.createFile(image);
      return this.postRepository.save({
        ...createPostDto,
        image: fileName,
      });
    } catch (error) {
      if (error instanceof QueryFailedError) {
        throw new BadRequestException('Something went wrong!');
      }
      throw error;
    }
  }

  public async findPostById(id: number) {
    try {
      const post = await this.postRepository.findOne({ where: { id } });
      if (!post) {
        throw new NotFoundException('Post not found!');
      }
      return post;
    } catch (error) {
      throw new NotFoundException('Post not found!');
    }
  }

  async findAllPosts(): Promise<Post[]> {
    try {
      return await this.postRepository.find({ relations: { comments: true } });
    } catch (error) {
      throw new NotFoundException('Post not found!');
    }
  }

  async updatePost(id: number, dto: UpdatePostDto, image: any): Promise<Post> {
    const fileName = await this.uploadService.createFile(image);
    const post = await this.findPostById(id);
    if (!post) {
      throw new NotFoundException(`Post with id: ${id} not found!`);
    }
    const updatedPost = Object.assign(post, dto);

    try {
      return await this.postRepository.save({
        ...updatedPost,
        image: fileName,
      });
    } catch (error) {
      if (error instanceof QueryFailedError && error.message.includes('name')) {
        throw new BadRequestException('Post name must be unique');
      }
      throw error;
    }
  }

  async deletePost(id: number): Promise<void> {
    try {
      await this.postRepository.delete(id);
    } catch (error) {
      throw new NotFoundException(`Post with id: ${id} not found!`);
    }
  }
}
