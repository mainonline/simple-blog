import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  UsePipes,
  UseInterceptors,
  Put,
  Delete,
  UploadedFile,
  ParseIntPipe,
  ValidationPipe,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { PostService } from './post.service';
import { CreatePostDto } from './dto/create-post.dto';
import { UpdatePostDto } from './dto/update-post.dto';

@Controller('post')
export class PostController {
  constructor(private readonly postService: PostService) {}

  @Post()
  @UseInterceptors(FileInterceptor('image'))
  @UsePipes(ValidationPipe)
  create(@Body() postDto: CreatePostDto, @UploadedFile() image) {
    return this.postService.createPost(postDto, image);
  }

  @Get(':id')
  findOne(@Param('id', ParseIntPipe) id: number) {
    return this.postService.findPostById(id);
  }

  @Get()
  findAll() {
    return this.postService.findAllPosts();
  }

  @Put('/:id')
  @UseInterceptors(FileInterceptor('image'))
  @UsePipes(ValidationPipe)
  update(
    @Param('id', ParseIntPipe) id: number,
    @Body() updatePostDto: UpdatePostDto,
    @UploadedFile() image,
  ) {
    return this.postService.updatePost(id, updatePostDto, image);
  }

  @Delete('/:id')
  delete(@Param('id', ParseIntPipe) id: number) {
    return this.postService.deletePost(id);
  }
}
