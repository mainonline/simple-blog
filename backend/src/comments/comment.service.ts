import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Comment } from './entities/comment.entity';
import { PostService } from 'src/posts/post.service';
import { Repository } from 'typeorm';
import { CreateCommentDto } from './dto/create-comment.dto';

@Injectable()
export class CommentService {
  constructor(
    @InjectRepository(Comment)
    private readonly commentRepository: Repository<Comment>,
    private postService: PostService,
  ) {}

  async createComment(dto: CreateCommentDto): Promise<Comment> {
    try {
      const post = await this.postService.findPostById(dto.postId);

      return await this.commentRepository.save({ ...dto, post });
    } catch (error) {
      throw new BadRequestException('Something went wrong!');
    }
  }
  async deleteComment(id: number): Promise<void> {
    await this.commentRepository.delete(id);
  }
}
