import {
  Controller,
  Post,
  Body,
  Param,
  UsePipes,
  ParseIntPipe,
  ValidationPipe,
  Delete,
} from '@nestjs/common';
import { CommentService } from './comment.service';
import { CreateCommentDto } from './dto/create-comment.dto';

@Controller('comment')
export class CommentController {
  constructor(private readonly commentService: CommentService) {}

  @Post()
  @UsePipes(ValidationPipe)
  create(@Body() dto: CreateCommentDto) {
    return this.commentService.createComment(dto);
  }

  @Delete('/:id')
  delete(@Param('id', ParseIntPipe) id: number) {
    return this.commentService.deleteComment(id);
  }
}
